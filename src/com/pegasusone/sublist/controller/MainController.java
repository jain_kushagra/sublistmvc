package com.pegasusone.sublist.controller;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pegasusone.sublist.pojo.SubList;
import com.pegasusone.sublist.service.EvaluateSubList;

@Controller
public class MainController {
	
	@Autowired
	private EvaluateSubList evaluateSubList;
	private static final Logger logger = Logger.getLogger(MainController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String displayLandingPage(Map<String, Object> model) {
		SubList subList = new SubList();
		model.put("subListForm", subList);
		return "index";
	}
	
	@RequestMapping(value = "/evaluateSubList", method = RequestMethod.POST)
	public String evaluateSubList(@ModelAttribute("subListForm") SubList subList,
            Map<String, Object> model) {
		
		logger.debug("Number List: " + subList.getNumberList());
		logger.debug("Sum: " + subList.getSum());
		
		Collections.sort(subList.getNumberList());
		List<Integer> outputSubList = new ArrayList<Integer>();
		outputSubList = evaluateSubList.getSubList(subList.getNumberList(), subList.getSum());
		model.put("numberList", subList.getNumberList());
		model.put("sum", subList.getSum());
		model.put("subList", outputSubList);
		return "result";
	}
}
