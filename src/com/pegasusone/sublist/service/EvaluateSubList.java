package com.pegasusone.sublist.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class EvaluateSubList {

	public List<Integer> getSubList(List<Integer> subList, Integer sum) {
		List<Integer> outputSubList = new ArrayList<Integer>();
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i <= subList.size(); i++) {
            int psum = subList.get(i);
            list.add(psum);
            for (int j = i + 1; j < subList.size(); j++) {
                if (psum + subList.get(j) == sum) {
                	list.add(subList.get(j));
                	outputSubList.addAll(list);
                	System.out.println(list);
                } else if (psum + subList.get(j) < sum) {
                	list.add(subList.get(j));
                	psum = psum + subList.get(j);
                } else {
                	list.clear();
                	break;
                }
            }
        }
		return outputSubList;
	}
}
